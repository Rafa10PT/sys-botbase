#include "dmnt_cheat_debug_events_manager.hpp"

extern "C" void continue_cheat_process(Handle cheat_dbg_hnd) {
	ams::dmnt::cheat::impl::ContinueCheatProcess(cheat_dbg_hnd);
}