#include <switch.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "commands.h"
#include "util.h"
#include "dmntcht.h"

void continue_cheat_process(Handle cheat_dbg_hnd);

Mutex actionLock;

//Controller:
bool bControllerIsInitialised = false;
u64 controllerHandle = 0;
HiddbgHdlsDeviceInfo controllerDevice = {0};
HiddbgHdlsState controllerState = {0};

Handle debughandle = 0;
u64 buttonClickSleepTime = 50;

DmntCheatProcessMetadata metaData;

bool is_attached = false;

extern u32 g_offset;

void attach()
{
	#if 0
    Result rc = dmntchtInitialize();
    if (R_FAILED(rc) && debugResultCodes)
        printf("dmntchtInitialize: %d\n", rc);
    rc = dmntchtForceOpenCheatProcess();
    if (R_FAILED(rc) && debugResultCodes)
        printf("dmntchtForceOpenCheatProcess: %d\n", rc);
    rc = dmntchtGetCheatProcessMetadata(&metaData);
    if (R_FAILED(rc) && debugResultCodes)
        printf("dmntchtGetCheatProcessMetadata: %d\n", rc);
	#endif
	
	if(is_attached)
		return;

	u64 pids[300];
    u32 numProc;
    svcGetProcessList((s32 *)&numProc, pids, 300);
    u64 pid = pids[numProc - 1 - g_offset];
	
	pminfoInitialize();
	
	pminfoGetProgramId(&metaData.title_id, numProc);
	
	pminfoExit();

    Result rc = svcDebugActiveProcess(&debughandle, pid);
    if (R_FAILED(rc))
    {
        printf("Couldn't open the process (Error: %x)\r\n"
               "Make sure that you actually started a game.\r\n",
               rc);
        return;
    }

	u8 plausibleReg = 0;
	bool heap = false;
	MemoryInfo meminfo;
	u32 pageinfo;
	u64 addr = 0;
	do {
		svcQueryDebugProcessMemory(&meminfo, &pageinfo, debughandle, addr);

		if(!(addr + meminfo.size))
			break;

		if(meminfo.type == 3 && meminfo.perm == 5 && plausibleReg < 2) {
			if(plausibleReg == 1) {
				metaData.main_nso_extents.base = meminfo.addr;
				metaData.main_nso_extents.size = meminfo.size;
			}
			++plausibleReg;
		}

		if(!heap && meminfo.type == MemType_Heap) {
			metaData.heap_extents.base = meminfo.addr;
			heap = true;
		}

		addr = meminfo.addr + meminfo.size;
	} while(addr != 0);
	
	is_attached = true;
	
	continue_cheat_process(debughandle);
}

void cont_ev() {
	if(!is_attached)
		return;
	
	continue_cheat_process(debughandle);
}

void detach() {
	if(!is_attached)
		return;
	
	svcCloseHandle(debughandle);
	debughandle = 0;
	
	is_attached = false;
}

void initController()
{
    if(bControllerIsInitialised) return;
    //taken from switchexamples github
    Result rc = hiddbgInitialize();
    if (R_FAILED(rc) && debugResultCodes)
        printf("hiddbgInitialize: %d\n", rc);
    // Set the controller type to Pro-Controller, and set the npadInterfaceType.
    controllerDevice.deviceType = HidDeviceType_FullKey3;
    controllerDevice.npadInterfaceType = XcdInterfaceType_Bluetooth;
    // Set the controller colors. The grip colors are for Pro-Controller on [9.0.0+].
    controllerDevice.singleColorBody = RGBA8_MAXALPHA(255,255,255);
    controllerDevice.singleColorButtons = RGBA8_MAXALPHA(0,0,0);
    controllerDevice.colorLeftGrip = RGBA8_MAXALPHA(230,255,0);
    controllerDevice.colorRightGrip = RGBA8_MAXALPHA(0,40,20);

    // Setup example controller state.
    controllerState.battery_level = 4; // Set battery charge to full.
    /*
	controllerState.joysticks[JOYSTICK_LEFT].dx = 0x0;
    controllerState.joysticks[JOYSTICK_LEFT].dy = -0x0;
    controllerState.joysticks[JOYSTICK_RIGHT].dx = 0x0;
    controllerState.joysticks[JOYSTICK_RIGHT].dy = -0x0;
    */
	rc = hiddbgAttachHdlsWorkBuffer();
    if (R_FAILED(rc) && debugResultCodes)
        printf("hiddbgAttachHdlsWorkBuffer: %d\n", rc);
    rc = hiddbgAttachHdlsVirtualDevice(&controllerHandle, &controllerDevice);
    if (R_FAILED(rc) && debugResultCodes)
        printf("hiddbgAttachHdlsVirtualDevice: %d\n", rc);
    bControllerIsInitialised = true;
}



void poke(u64 offset, u64 size, u8* val)
{
	#if 0
    Result rc = dmntchtWriteCheatProcessMemory(offset, val, size);
    if (R_FAILED(rc) && debugResultCodes)
        printf("dmntchtWriteCheatProcessMemory: %d\n", rc);
	#endif
	
	Result rc = svcWriteDebugProcessMemory(debughandle, val, offset, size);
	if(R_FAILED(rc))
		printf("svcWriteDebugProcessMemory: %d\n", rc);
}

void peek(u64 offset, u64 size)
{
    u8 out[size];
	#if 0
    Result rc = dmntchtReadCheatProcessMemory(offset, &out, size);
    if (R_FAILED(rc) && debugResultCodes)
        printf("dmntchtReadCheatProcessMemory: %d\n", rc);
	#endif
	
	Result rc = svcReadDebugProcessMemory(&out, debughandle, offset, size);
	
	if(R_FAILED(rc)) {
		printf("svcReadDebugProcessMemory: %d\n", rc);
		return;
	}

    u64 i;
    for (i = 0; i < size; i++)
    {
        printf("%02X", out[i]);
    }
    printf("\n");
}

/*
void click(HidControllerKeys btn)
{


}
void press(HidControllerKeys btn)
{

}

void release(HidControllerKeys btn)
{
}
*/

void setStickState(int side, int dxVal, int dyVal)
{
}