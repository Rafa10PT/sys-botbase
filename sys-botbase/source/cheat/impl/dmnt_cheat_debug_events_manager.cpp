/*
 * Copyright (c) 2018-2020 Atmosphère-NX
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "dmnt_cheat_debug_events_manager.hpp"
#include "dmnt_cheat_hardware_breakpoint.hpp"

/* WORKAROUND: This design prevents a kernel deadlock from occurring on 6.0.0+ */

extern "C" {
	void __wrap___cxa_allocate_exception() {*(u64 *)(0) = 69;}
	void __wrap___cxa_throw() {*(u64 *)(0) = 69;}
	void __wrap__Unwind_Resume() {*(u64 *)(0) = 69;}
	void __wrap___gxx_personality_v0() {*(u64 *)(0) = 69;}
	/*
	void __wrap__ZSt19__throw_logic_errorPKc() {*(u64 *)(0) = 69;}
	void __wrap__ZSt20__throw_length_errorPKc() {*(u64 *)(0) = 69;}
	void __wrap__ZSt19__throw_logic_errorPKc() {*(u64 *)(0) = 69;}
	void __wrap__ZSt20__throw_length_errorPKc() {*(u64 *)(0) = 69;}
	*/
}

namespace ams {

    ncm::ProgramId CurrentProgramId = ncm::SystemProgramId::Dmnt;

    namespace result {

        bool CallFatalOnResultAssertion = true;

    }

}

u64 g_bp_vect[100] = {};
extern ams::os::Mutex message_board_access;
extern "C" u64 message_board[4];
extern "C" ThreadContext ctx;
namespace ams::dmnt::cheat::impl {

    namespace {

        class DebugEventsManager {
            public:
                static constexpr size_t NumCores = 4;
                static constexpr size_t ThreadStackSize = os::MemoryPageSize;
                static constexpr size_t ThreadPriority = 24;
            private:
				std::array<uintptr_t, NumCores> message_queue_buffers;
                std::array<os::MessageQueue, NumCores> message_queues;
				std::array<os::ThreadType, NumCores> threads;
                os::Event continued_event;

                alignas(os::MemoryPageSize) u8 thread_stacks[NumCores][ThreadStackSize];
            private:
                static void PerCoreThreadFunction(void *_this) {
                    /* This thread will wait on the appropriate message queue. */
                    DebugEventsManager *this_ptr = reinterpret_cast<DebugEventsManager *>(_this);
                    const size_t current_core = svcGetCurrentProcessorNumber();
                    while (true) {
                        /* Receive handle. */
                        // Handle debug_handle = this_ptr->WaitReceiveHandle(current_core);
						u64 val = this_ptr->WaitReceiveHandle(current_core);

						u32 pos = val >> 32;
						Handle debug_handle = static_cast<Handle>(val);
						if(pos >> 31) {
							#if 0
							message_board_access.lock();
							u32 thetid = ctx.pc.x;
							u32 thehand = ctx.lr;
							armDCacheFlush((void *)&ctx, sizeof(ThreadContext));
							volatile u32 ff = (volatile u32)(thehand & 0xFFFFFFFF);
							Result rc = svcGetDebugThreadContext(&ctx, /*debug_handle*/ff, /*pos & 0x7FFFFFFF*/thetid, 15);
							__asm__ __volatile__(
								"mov x15, %[ti]\n"
								"mov x8, %[rpc]\n"
								"mov x9, %[rfloat]\n"
								"mov x10, %[rc]\n"
								"mov x11, %[tid]\n"
								"mov x12, %[posn]\n"
								"mov x13, %[h]\n"
								"mov x14, %[th]\n"
								"mov x0, #0\n"
								"br x0\n"
								:: [rpc] "r" (ctx.pc.r), [rfloat] "r" (ctx.fpcr),
								[rc] "r" (rc), [tid] "r" (pos & 0x7FFFFFFF), [posn] "r" (pos),
								[h] "r" ((debug_handle << 32) >> 32), [th] "r" (ff), [ti] "r" (thetid)
							);
							armDCacheFlush((void *)&ctx, sizeof(ThreadContext));
							message_board_access.unlock();
							#endif
						}
						else if(pos != 0) {
							message_board_access.lock();
							
							armDCacheFlush((void *)g_bp_vect, 100 * sizeof(uintptr_t));
							armDCacheFlush((void *)(g_bp_vect[pos]), sizeof(HWBreakpoint));
							
							armDCacheFlush((void *)(message_board), 4 * sizeof(u64));
							
							svcSetHardwareBreakPoint(5, 0x3001E1ULL, debug_handle);
							
							HWBreakpoint *bp = reinterpret_cast<HWBreakpoint *>(g_bp_vect[pos]);
							
							svcSetHardwareBreakPoint(bp->id, bp->cr, !bp->vr ? bp->address : bp->vr);
							
							u64 temp[] = {bp->id, bp->cr, !bp->vr ? bp->address : bp->vr, svcGetCurrentProcessorNumber()};
							memcpy(message_board, temp, 4 * sizeof(u64));
							
							/*
							__asm__ __volatile__(
								"mov x23, %[b1]\n"
								"mov x24, %[b2]\n"
								"mov x25, %[b3]\n"
								"mov x26, %[b4]\n"
								"mov x0, #0\n"
								"br x0\n"
								:
								: [b1] "r" (temp[0]), [b2] "r" (temp[1]), [b3] "r" (temp[2]), [b4] "r" (temp[3])
								:
							);
							*/
							
							armDCacheFlush((void *)(g_bp_vect[pos]), sizeof(HWBreakpoint));
							
							for(u32 i = pos; i < g_bp_vect[g_bp_vect[0]]; ++i) {
								g_bp_vect[i] = g_bp_vect[i + 1];
							}
							
							--g_bp_vect[0];
							
							armDCacheFlush((void *)g_bp_vect, 100 * sizeof(uintptr_t));
							
							armDCacheFlush((void *)(message_board), 4 * sizeof(u64));
							
							message_board_access.unlock();
						}

                        /* Continue events on the correct core. */
                        R_ABORT_UNLESS(this_ptr->ContinueDebugEvent(debug_handle));

                        /* Signal that we've continued. */
                        this_ptr->SignalContinued();
                    }
                }

                size_t GetTargetCore(const svc::DebugEventInfo &dbg_event, Handle debug_handle) {
                    /* If we don't need to continue on a specific core, use the system core. */
                    size_t target_core = NumCores - 1;

                    /* Retrieve correct core for new thread event. */
                    if (dbg_event.type == svc::DebugEvent_CreateThread) {
                        u64 out64 = 0;
                        u32 out32 = 0;
                        R_ABORT_UNLESS(svcGetDebugThreadParam(&out64, &out32, debug_handle, dbg_event.info.create_thread.thread_id, DebugThreadParam_CurrentCore));
                        target_core = out32;
                    }

                    return target_core;
                }

                void SendHandle(size_t target_core, u64 debug_handle) {
                    this->message_queues[target_core].Send(static_cast<uintptr_t>(debug_handle));
                }

                uintptr_t WaitReceiveHandle(size_t core_id) {
                    uintptr_t x = 0;
                    this->message_queues[core_id].Receive(&x);
                    return x;// return static_cast<Handle>(x);
                }

                Result ContinueDebugEvent(Handle debug_handle) {
                    // Calling hos::GetVerion() calls secmon and we may not be on core 3.
					// We can talk to spl to get the version for us.
					// We can also call this on appInit, so as to set a cached value.
					if (hos::GetVersion() >= hos::Version_3_0_0) {
                        return svcContinueDebugEvent(debug_handle, 7, nullptr, 0);
                    } else {
                        return svcLegacyContinueDebugEvent(debug_handle, 7, 0);
                    }
					
					return svcContinueDebugEvent(debug_handle, 7, nullptr, 0);
                }

                void WaitContinued() {
                    this->continued_event.Wait();
                }

                void SignalContinued() {
                    this->continued_event.Signal();
                }

            public:
                DebugEventsManager() : message_queues{os::MessageQueue(std::addressof(message_queue_buffers[0]), 1), os::MessageQueue(std::addressof(message_queue_buffers[1]), 1), os::MessageQueue(std::addressof(message_queue_buffers[2]), 1), os::MessageQueue(std::addressof(message_queue_buffers[3]), 1)}, continued_event(os::EventClearMode_AutoClear), thread_stacks{} {
					armDCacheFlush((void *)g_bp_vect, 100 * sizeof(u64));
					for (size_t i = 0; i < NumCores; i++) {
                        /* Create thread. */
                        R_ABORT_UNLESS(os::CreateThread(std::addressof(this->threads[i]), PerCoreThreadFunction, this, this->thread_stacks[i], ThreadStackSize, AMS_GET_SYSTEM_THREAD_PRIORITY(dmnt, MultiCoreEventManager), i));

                        /* Set core mask. */
                        os::SetThreadCoreMask(std::addressof(this->threads[i]), i, (1u << i));

                        /* Start thread. */
                        os::StartThread(std::addressof(this->threads[i]));
                    }
                }
				
				static void ReadThreadContext(Handle cheat_dbg_hnd, u64 thread_id) {
					message_board_access.lock();
					armDCacheFlush((void *)&ctx, sizeof(ThreadContext));
					
					R_ABORT_UNLESS(svcGetDebugThreadContext(&ctx, cheat_dbg_hnd, thread_id, 15));
					
					armDCacheFlush((void *)&ctx, sizeof(ThreadContext));
					message_board_access.unlock();				
				}

                u32 ContinueCheatProcess(Handle cheat_dbg_hnd) {
                    /* Loop getting all debug events. */
                    svc::DebugEventInfo d;
                    size_t target_core = NumCores - 1;
					u32 isEnabled = 0;
                    while (R_SUCCEEDED(svcGetDebugEvent(reinterpret_cast<u8 *>(&d), cheat_dbg_hnd))) {
                        if (d.type == svc::DebugEvent_CreateThread || d.type == svc::DebugEvent_Exception)
                            target_core = GetTargetCore(d, cheat_dbg_hnd);
						if(d.type == svc::DebugEvent_Exception && d.info.exception.type == svc::DebugException_BreakPoint){
							ReadThreadContext(cheat_dbg_hnd, d.thread_id);
							isEnabled = 1;
						}
                    }
					
					if(d.type == svc::DebugEvent_Exception && d.info.exception.type == svc::DebugException_BreakPoint){
						ReadThreadContext(cheat_dbg_hnd, d.thread_id);
						isEnabled = 1;
					}

                    /* Send handle to correct core, wait for continue to finish. */
                    this->SendHandle(target_core, /*d.type == svc::DebugEvent_Exception && d.info.exception.type == svc::DebugException_BreakPoint ? 0x8000000000000000| d.info.attach_thread.thread_id << 32 | cheat_dbg_hnd : cheat_dbg_hnd*/cheat_dbg_hnd);
                    this->WaitContinued();
					return isEnabled;
                }
				
				os::MessageQueue &GetMessageQueues(u32 core) {
					return message_queues[core];
				}
        };

        /* Manager global. */
        DebugEventsManager g_events_manager;
    }

    u32 ContinueCheatProcess(Handle cheat_dbg_hnd) {
        return g_events_manager.ContinueCheatProcess(cheat_dbg_hnd);
    }
	
	void SendVal(u64 to_send, u32 core) {
		g_events_manager.GetMessageQueues(core).Send(static_cast<uintptr_t>(to_send));
	}

}
