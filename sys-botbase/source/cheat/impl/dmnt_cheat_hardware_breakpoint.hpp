// Taken from Comex's github.
// https://github.com/comex/twili

#include <stratosphere.hpp>

namespace ams::dmnt::cheat::impl {
	
	namespace {
		class HWBreakpoint {
		 public:
			bool ValidateAndComputeRegs();
			const char *TypeStr() const;
			bool HasLinkedContextIDBreakpoint() const;
			bool Matches(const HWBreakpoint &) const;
			void SetTo(const HWBreakpoint &);
			void SendHandle(size_t target_core, Handle debug_handle);

			int8_t id = -1; // breakpoints: 0-5; watchpoints: 0x10-0x13

			enum class Type {
				Disabled,
				Break,
				WatchR,
				WatchW,
				WatchRW,
				ContextID,
			} type = Type::Disabled;

			uint64_t pid = 0;

			// for everything other than ContextID:
			uint64_t address = 0;
			uint64_t size = 0;
			int8_t linked_contextid_bp_id = -1;
			// for ContextID:
			int8_t refcount = 0;

			// calculated:
			uint32_t cr = 0;
			uint64_t vr = 0;
		};
	}
	
	// We are adding hardware breakpoint support to the debugger.
	u32 SetHardwareBreakPoint(u64 value, u64 size, u32 type, u32 core, Handle cheat_dbg_hnd);
	
	constexpr u32 contextID = 5;
	
	std::pair<HWBreakpoint, bool> breakpoints[10];
}
