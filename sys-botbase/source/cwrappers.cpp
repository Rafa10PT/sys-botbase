#include "dmnt_cheat_debug_events_manager.hpp"
#include "dmnt_cheat_hardware_breakpoint.hpp"
#include <stratosphere.hpp>
using namespace ams;

ams::os::Mutex message_board_access(false);

extern "C" {
	u32 continue_cheat_process(Handle cheat_dbg_hnd) {
		return ams::dmnt::cheat::impl::ContinueCheatProcess(cheat_dbg_hnd);
	}

	void hos_initialize_for_stratosphere() {
		ams::hos::InitializeForStratosphere();
	}
	
	u32 set_hardware_break_point(u64 value, u64 size, u32 type, u32 core, Handle debughandle) {
		return ams::dmnt::cheat::impl::SetHardwareBreakPoint(value, size, type, core, debughandle);
	}
	
	void mlock() {
		message_board_access.lock();
	}
	
	void munlock() {
		message_board_access.unlock();
	}
	u32 isBreakpoint(Handle debughandle){
		svc::DebugEventInfo d;
		svcGetDebugEvent(&d, debughandle);
		return d.type == svc::DebugEvent_Exception && d.info.exception.type == svc::DebugException_BreakPoint;
	}
	
	u32 checkDebugEventResult(Handle debughandle){
		svc::DebugEventInfo d;
		u32 rc;
		rc = svcGetDebugEvent(&d, debughandle);
		if(rc == 0)
			return 0;
		return 1;
	}
}