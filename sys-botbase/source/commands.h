#include <switch.h>
#include "dmntcht.h"

extern Handle debughandle;
extern bool bControllerIsInitialised;
extern u64 controllerHandle;
/*
extern HiddbgHdlsDeviceInfo controllerDevice;
extern HiddbgHdlsState controllerState;
*/
extern u64 buttonClickSleepTime;

extern DmntCheatProcessMetadata metaData;

void attach();
void detach();
void cont_ev();

void poke(u64 offset, u64 size, u8* val);
void peek(u64 offset, u64 size);
/*
void click(HidControllerKeys btn);
void press(HidControllerKeys btn);
void release(HidControllerKeys btn);
*/
void setStickState(int side, int dxVal, int dyVal);