// Taken from Comex's github.
// https://github.com/comex/twili
#include "dmnt_cheat_hardware_breakpoint.hpp"
#include "dmnt_cheat_debug_events_manager.hpp"

extern u64 g_bp_vect[100];

namespace ams::dmnt::cheat::impl {
	void SendVal(u64 to_send, u32 core);
	namespace {
		bool HWBreakpoint::ValidateAndComputeRegs() {
			switch(type) {
			case Type::Disabled:
				vr = 0;
				cr = 0;
				return true;

			case Type::Break: {
				if(size != 4 || (address & 3) || linked_contextid_bp_id < 0) {
					return false;
				}
				vr = address;
				uint32_t bt = 1; // type = linked instruction address match
				uint32_t lbn = (uint32_t)linked_contextid_bp_id;
				uint32_t bas = 0xf;
				uint32_t e = 1; // enabled
				cr = bt << 20 | lbn << 16 | bas << 5 | e << 0;
				return true;
			}

			case Type::WatchR:
			case Type::WatchW:
			case Type::WatchRW: {
				if(size <= 8) {
					// Any size <= 8 can be implemented with the BAS field as long as
					// the start and end are within the same u64.
					if((address & 7) + size > 8) {
						return false;
					}
					vr = address & ~7;
					uint32_t bas = ((1 << size) - 1) << (address & 7);
					cr = bas << 5;
				} else if (size <= 0x80000000) {
					// Larger watches up to 2GB can be implemented with MASK if the
					// size is a power of 2 and the address is aligned to that size.
					if((size & (size - 1)) != 0 ||
						(address & (size - 1)) != 0) {
						return false;
					}
					uint32_t mask = 0;
					for(uint64_t test = 1; test != size; test *= 2) {
						// 1st: test = 1 -- mask + 1 = 2 -- test * 2 = 2
						// 2nd: test = 2 -- mask + 1 = 3 -- test * 2 = 4
						// 3rd: test = 4 -- mask + 1 = 4 -- test * 2 = 8
						mask++;
					}
					cr = mask << 24;
				}
				if(linked_contextid_bp_id < 0) {
					return false;
				}
				uint32_t lbn = (uint32_t)linked_contextid_bp_id;
				uint32_t lsc =
					(type != Type::WatchW ? 1 : 0) | // load
					(type != Type::WatchR ? 2 : 0); // store
				uint32_t e = 1; // enabled
				cr |= lbn << 16 | lsc << 3 | e << 0;
				return true;
			}

			case Type::ContextID: {
				uint32_t bt = 3; // type = linked ContextID match
				uint32_t bas = 0xf;
				uint32_t e = 1; // enabled
				cr = bt << 20 | bas << 5 | e << 0;
				return true;
			}
			}
		}
		
		void HWBreakpoint::SendHandle(size_t target_core, Handle debug_handle) {
			armDCacheFlush((void *)(this), sizeof(HWBreakpoint));
			armDCacheFlush((void *)g_bp_vect, 100 * sizeof(u64));
			
			g_bp_vect[++g_bp_vect[0]] = reinterpret_cast<u64>(this);
			
			u64 val = g_bp_vect[0] << 32 | debug_handle;
			
			armDCacheFlush((void *)(this), sizeof(HWBreakpoint));
			armDCacheFlush((void *)g_bp_vect, 100 * sizeof(u64));
			
			u32 idc = ams::dmnt::cheat::impl::ContinueCheatProcess(debug_handle);
			auto a = SendVal;
			auto b = ams::dmnt::cheat::impl::SendVal;
			SendVal(val, target_core);
		}
	}
	
	u32 SetHardwareBreakPoint(u64 value, u64 size, u32 type, u32 core, Handle cheat_dbg_hnd) {
		int limit = (type >> 1) != 0 ? 0x14 : 5;
		for(int i = (type >> 1) != 0 ? 6 : 0; i < limit; ++i) {
			if(!breakpoints[i].second) {
				HWBreakpoint *bp = &breakpoints[i].first;
				breakpoints[i].second = true;
				bp->address = value;
				bp->type = static_cast<HWBreakpoint::Type>(type);
				bp->linked_contextid_bp_id = contextID;
				bp->size = size;
				bp->id = i > 5 ? i + 10 : i;
				bp->SendHandle(core, cheat_dbg_hnd);
				return !bp->ValidateAndComputeRegs() ? 0x14 : (i > 5 ? i + 10 : i);
			}
		}
		return 0x15;
	}
}
